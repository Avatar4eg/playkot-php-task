<?php

namespace Avatar4eg\PhpTestTask\Payment;

interface IPayment
{
    /**
     * @param string $paymentId
     * @param \DateTimeInterface $created
     * @param \DateTimeInterface $updated
     * @param bool $isTest
     * @param Currency $currency
     * @param float $amount
     * @param float $taxAmount
     * @param State $state
     * @return IPayment
     */
    public static function instance(
        string                  $paymentId,
        \DateTimeInterface      $created,
        \DateTimeInterface      $updated,
        bool                    $isTest,
        Currency                $currency,
        float                   $amount,
        float                   $taxAmount,
        State                   $state
    ): IPayment;

    /**
     * Идентификатор платежа
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Изменение идентификатора платежа
     *
     * @param string $paymentId
     */
    public function setId(string $paymentId);

    /**
     * Дата создания платежа
     *
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface;

    /**
     * Изменение даты создания платежа
     *
     * @param \DateTimeInterface $created
     */
    public function setCreated(\DateTimeInterface $created);

    /**
     * Дата последнего обновления платежа
     *
     * @return \DateTimeInterface
     */
    public function getUpdated(): \DateTimeInterface;

    /**
     * Изменение даты последнего обновления платежа
     *
     * @param \DateTimeInterface $updated
     */
    public function setUpdated(\DateTimeInterface $updated);

    /**
     * Признак тестового платежа
     *
     * @return bool
     */
    public function isTest(): bool;

    /**
     * Изменение признака тестового платежа
     *
     * @param bool $isTest
     */
    public function setIsTest(bool $isTest);

    /**
     * Валюта платежа
     *
     * @return Currency
     */
    public function getCurrency(): Currency;

    /**
     * Изменение валюты платежа
     *
     * @param Currency $currency
     */
    public function setCurrency(Currency $currency);

    /**
     * Сумма платежа включая сумму налога
     *
     * @return float
     */
    public function getAmount(): float;

    /**
     * Изменение суммы платежа
     *
     * @param float $amount
     */
    public function setAmount(float $amount);

    /**
     * Сумма налога от платежа
     *
     * @return float
     */
    public function getTaxAmount(): float;

    /**
     * Изменение суммы налога от платежа
     *
     * @param float $taxAmount
     */
    public function setTaxAmount(float $taxAmount);

    /**
     * Идентификатор состояния платежа
     *
     * @return State
     */
    public function getState(): State;

    /**
     * Изменение идентификатора состояния платежа
     *
     * @param State $state
     */
    public function setState(State $state);
}
