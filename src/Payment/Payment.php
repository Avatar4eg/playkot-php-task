<?php

namespace Avatar4eg\PhpTestTask\Payment;

use MongoDB\BSON\Persistable;

class Payment implements IPayment, Persistable
{
    /** @var string $paymentId */
    private $paymentId;

    /** @var \DateTimeInterface $created */
    private $created;

    /** @var \DateTimeInterface $updated */
    private $updated;

    /** @var bool $isTest */
    private $isTest;

    /** @var Currency $currency */
    private $currency;

    /** @var float $amount */
    private $amount;

    /** @var float $taxAmount */
    private $taxAmount;

    /** @var State $state */
    private $state;

    /**
     * Payment constructor.
     *
     * @param string $paymentId
     * @param \DateTimeInterface $created
     * @param \DateTimeInterface $updated
     * @param bool $isTest
     * @param Currency $currency
     * @param float $amount
     * @param float $taxAmount
     * @param State $state
     * @throws \InvalidArgumentException
     */
    private function __construct(
        string $paymentId,
        \DateTimeInterface $created,
        \DateTimeInterface $updated,
        bool $isTest,
        Currency $currency,
        float $amount,
        float $taxAmount,
        State $state
    ) {
        $this->setId($paymentId)
            ->setCreated($created)
            ->setUpdated($updated)
            ->setIsTest($isTest)
            ->setCurrency($currency)
            ->setAmount($amount)
            ->setTaxAmount($taxAmount)
            ->setState($state);
    }

    /**
     * @param string $paymentId
     * @param \DateTimeInterface $created
     * @param \DateTimeInterface $updated
     * @param bool $isTest
     * @param Currency $currency
     * @param float $amount
     * @param float $taxAmount
     * @param State $state
     * @return IPayment
     * @throws \InvalidArgumentException
     */
    public static function instance(
        string $paymentId,
        \DateTimeInterface $created,
        \DateTimeInterface $updated,
        bool $isTest,
        Currency $currency,
        float $amount,
        float $taxAmount,
        State $state
    ): IPayment {
        return new self(
            $paymentId,
            $created,
            $updated,
            $isTest,
            $currency,
            $amount,
            $taxAmount,
            $state
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->paymentId;
    }

    /**
     * @param string $paymentId
     * @return Payment
     * @throws \InvalidArgumentException
     */
    public function setId(string $paymentId): Payment
    {
        if ('' === $paymentId) {
            throw new \InvalidArgumentException('Payment ID cant be null');
        }
        $this->paymentId = $paymentId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param \DateTimeInterface $created
     * @return Payment
     */
    public function setCreated(\DateTimeInterface $created): Payment
    {
        $this->created = clone $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param \DateTimeInterface $updated
     * @return Payment
     */
    public function setUpdated(\DateTimeInterface $updated): Payment
    {
        $this->updated = clone $updated;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTest(): bool
    {
        return $this->isTest;
    }

    /**
     * @param bool $isTest
     * @return Payment
     */
    public function setIsTest(bool $isTest): Payment
    {
        $this->isTest = $isTest;
        return $this;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     * @return Payment
     */
    public function setCurrency(Currency $currency): Payment
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Payment
     * @throws \InvalidArgumentException
     */
    public function setAmount(float $amount): Payment
    {
        if ($amount < 0) {
            throw new \InvalidArgumentException('Amount cant be below 0');
        }
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getTaxAmount(): float
    {
        return $this->taxAmount;
    }

    /**
     * @param float $taxAmount
     * @return Payment
     * @throws \InvalidArgumentException
     */
    public function setTaxAmount(float $taxAmount): Payment
    {
        if ($taxAmount < 0) {
            throw new \InvalidArgumentException('Tax amount cant be below 0');
        }
        $this->taxAmount = $taxAmount;
        return $this;
    }

    /**
     * @return State
     */
    public function getState(): State
    {
        return $this->state;
    }

    /**
     * @param State $state
     * @return Payment
     */
    public function setState(State $state): Payment
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return array|object
     */
    public function bsonSerialize()
    {
        return [
            'paymentId' => $this->getId(),
            'created'   => $this->getCreated()->format('Y-m-d H:i:s'),
            'updated'   => $this->getUpdated()->format('Y-m-d H:i:s'),
            'isTest'    => $this->isTest(),
            'currency'  => $this->getCurrency()->getCode(),
            'amount'    => $this->getAmount(),
            'taxAmount' => $this->getTaxAmount(),
            'state'     => $this->getState()->getCode(),
        ];
    }

    /**
     * @param array $data
     */
    public function bsonUnserialize(array $data)
    {
        try {
            $this->setId($data['paymentId'])
                ->setCreated(new \DateTime($data['created']))
                ->setUpdated(new \DateTime($data['updated']))
                ->setIsTest($data['isTest'])
                ->setCurrency(new Currency($data['currency']))
                ->setAmount($data['amount'])
                ->setTaxAmount($data['taxAmount'])
                ->setState(new State($data['state']));
        } catch (\InvalidArgumentException $exception) {
            $this->{'message'} = 'Failed load from DB. ' . $exception->getMessage();
        }
    }

    /**
     * @param string $data
     * @return Payment
     * @throws \RuntimeException
     */
    public static function stringUnserialize(string $data): self
    {
        $payment = unserialize(
            $data,
            ['allowed_classes' => [self::class, \DateTime::class, Currency::class, State::class]]
        );
        if (!($payment instanceof self)) {
            throw new \RuntimeException('Cant unserialize data.');
        }
        return $payment;
    }
}
