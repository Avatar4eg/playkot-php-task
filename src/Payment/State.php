<?php

namespace Avatar4eg\PhpTestTask\Payment;

final class State
{
    const CREATED   = 0; // Платёж создан но не проведён
    const CHARGED   = 1; // Платёж успешно проведён
    const DECLINED  = 2; // Платёж отменён
    const REFUNDED  = 3; // Платёж возвращён

    private static $states = [
        self::CREATED   => null,
        self::CHARGED   => null,
        self::DECLINED  => null,
        self::REFUNDED  => null,
    ];

    private static $stateNames = [
        self::CREATED   => 'Create',
        self::CHARGED   => 'Charge',
        self::DECLINED  => 'Decline',
        self::REFUNDED  => 'Refund',
    ];

    /** @var int */
    private $code;

    /**
     * @param int $code
     */
    public function __construct(int $code)
    {
        if (!self::exists($code)) {
            throw new \InvalidArgumentException('Undefined payment state ' . $code);
        }

        $this->code = $code;
    }

    /**
     * @param int $code
     * @return bool
     */
    public static function exists(int $code): bool
    {
        return array_key_exists($code, self::$states);
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function existsByName(string $name): bool
    {
        return \in_array($name, self::$stateNames, true);
    }

    /**
     * @param string $code
     * @return State
     */
    public static function get(string $code): State
    {
        if (self::$states[$code] === null) {
            self::$states[$code] = new self($code);
        }

        return self::$states[$code];
    }

    /**
     * @param string $name
     * @return int
     */
    public static function getCodeByName(string $name): int
    {
        $names = array_flip(self::$stateNames);
        if (null === $names) {
            return null;
        }
        return $names[$name] ?: null;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }
}
