<?php

namespace Avatar4eg\PhpTestTask\Handler;

use Avatar4eg\PhpTestTask\Payment\IPayment;

interface IHandler
{
    /**
     * @param array $event
     * @return IPayment
     */
    public function process(array $event): IPayment;
}
