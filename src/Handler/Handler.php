<?php

namespace Avatar4eg\PhpTestTask\Handler;

use Avatar4eg\PhpTestTask\Payment\Currency;
use Avatar4eg\PhpTestTask\Payment\Payment;
use Avatar4eg\PhpTestTask\Payment\IPayment;
use Avatar4eg\PhpTestTask\Payment\State;
use Avatar4eg\PhpTestTask\Storage\Exception\NotFound;
use Avatar4eg\PhpTestTask\Storage\Exception\Payment as PaymentException;
use Avatar4eg\PhpTestTask\Storage\Exception\Storage;
use Avatar4eg\PhpTestTask\Storage\IStorage;
use Avatar4eg\PhpTestTask\Storage\StorageFactory;

class Handler implements IHandler
{
    private static $required_fields = [
        'id',
        'currency',
        'value',
        'action',
    ];

    /** @var IStorage $storage */
    protected $storage;

    /**
     * BaseHandler constructor.
     * @param array $config
     * @throws PaymentException
     */
    public function __construct(array $config)
    {
        $this->storage = StorageFactory::create($config);
    }

    /**
     * @param array $event
     * @return IPayment
     * @throws PaymentException
     */
    public function process(array $event): IPayment
    {
        if (\count(array_intersect_key(array_flip(self::$required_fields), $event)) !== \count(self::$required_fields)) {
            throw new PaymentException('Bad data. Event doesnt contain all required fields.');
        }

        if (!Currency::exists($event['currency'])) {
            throw new PaymentException('Bad data. Cant convert currency: ' . $event['currency']);
        }
        if (!State::existsByName($event['action'])
            || null === $state_code = State::getCodeByName($event['action'])) {
            throw new PaymentException('Bad data. Cant convert state: ' . $event['action']);
        }

        $value = (float)str_replace(',', '.', $event['value']);
        $tax = array_key_exists('tax', $event)
            ? (float)str_replace(',', '.', $event['tax'])
            : 0.0;
        $is_test = array_key_exists('isTest', $event) ? (bool)$event['isTest'] : false;

        /** @var Payment $payment */
        try {
            // Another option - check $storage->has and then $storage->get. But this option has same amount
            // of requests to DB on update and on create - that's more predictable.
            $payment = $this->storage->get($event['id']);
            $payment->setUpdated(new \DateTime())
                ->setState(new State($state_code))
                ->setIsTest($is_test)
                ->setCurrency(new Currency($event['currency']))
                ->setAmount($value)
                ->setTaxAmount($tax)
                ->setState(new State($state_code));
        } catch (NotFound $exception) {
            try {
                $payment = Payment::instance(
                    $event['id'],
                    new \DateTime(),
                    new \DateTime(),
                    $is_test,
                    new Currency($event['currency']),
                    $value,
                    $tax,
                    new State($state_code)
                );
            } catch (\InvalidArgumentException $exception) {
                throw new PaymentException('Error creating new payment. ' . $exception->getMessage());
            }
        } catch (\InvalidArgumentException $exception) {
            throw new PaymentException('Error updating payment. ' . $exception->getMessage());
        }

        try {
            $this->storage->save($payment);
        } catch (Storage $exception) {
            throw new PaymentException('Error saving payment. ' . $exception->getMessage());
        }

        return $payment;
    }
}
