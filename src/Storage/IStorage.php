<?php

namespace Avatar4eg\PhpTestTask\Storage;

use Avatar4eg\PhpTestTask\Payment\IPayment;
use Avatar4eg\PhpTestTask\Storage\Exception;

interface IStorage
{
    /**
     * Фабричный метод для создания экземпляра хранилища
     *
     * @param array $config
     * @return IStorage
     * @throws Exception\Storage
     */
    public static function instance(array $config = null): IStorage;

    /**
     * Сохранение существующего платежа или создание нового
     *
     * @param IPayment $payment
     * @return IStorage
     * @throws Exception\Storage
     */
    public function save(IPayment $payment): IStorage;

    /**
     * Проверка на существование платежа
     *
     * @param string $paymentId
     * @return bool
     */
    public function has(string $paymentId): bool;

    /**
     * Получение платежа
     *
     * @param string $paymentId
     * @return IPayment
     * @throws Exception\NotFound
     */
    public function get(string $paymentId): IPayment;

    /**
     * Удаление платежа
     *
     * @param IPayment $payment
     * @return IStorage
     */
    public function remove(IPayment $payment): IStorage;
}
