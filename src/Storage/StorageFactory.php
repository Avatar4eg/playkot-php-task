<?php

namespace Avatar4eg\PhpTestTask\Storage;

use Avatar4eg\PhpTestTask\Storage\Exception\Payment as PaymentException;
use Avatar4eg\PhpTestTask\Storage\Exception\Storage;

class StorageFactory
{
    /**
     * @param array $config
     * @return IStorage
     * @throws PaymentException
     */
    public static function create(array $config)
    {
        switch ($config['db_type']) {
            case 'mongodb':
                try {
                    $storage = MongoStorage::instance($config['mongodb']);
                } catch (Storage $exception) {
                    throw new PaymentException('Storage connection error. ' . $exception->getMessage());
                }
                return $storage;
            case 'redis':
                try {
                    $storage = RedisStorage::instance($config['redis']);
                } catch (Storage $exception) {
                    throw new PaymentException('Storage connection error. ' . $exception->getMessage());
                }
                return $storage;
            default:
                throw new PaymentException('No DB configuration provided');
        }
    }
}
