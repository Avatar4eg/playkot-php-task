<?php

namespace Avatar4eg\PhpTestTask\Storage;

use Avatar4eg\PhpTestTask\Payment\IPayment;
use Avatar4eg\PhpTestTask\Payment\Payment;
use Redis;

class RedisStorage implements IStorage
{
    /** @var Redis $connection */
    private $connection;

    /**
     * MongoStorage constructor.
     *
     * @param array|null $config
     * @throws Exception\Storage
     */
    private function __construct(array $config = null)
    {
        try {
            $this->connection = new Redis();
            if (null === $config) {
                $client = $this->connection->connect(
                    'localhost',
                    '6379'
                );
            } else {
                $client = $this->connection->connect(
                    $config['host'],
                    $config['port']
                );
            }
        } catch (\RedisException $exception) {
            throw new Exception\Storage('Connection creation error. ' . $exception->getMessage());
        }

        if (false === $client) {
            throw new Exception\Storage('Connection creation error.');
        }
    }

    /**
     * Фабричный метод для создания экземпляра хранилища
     *
     * @param array $config
     * @return IStorage
     * @throws Exception\Storage
     */
    public static function instance(array $config = null): IStorage
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new self($config);
        }

        return $inst;
    }

    /**
     * Сохранение существующего платежа или создание нового
     *
     * @param IPayment $payment
     * @return IStorage
     * @throws Exception\Storage
     */
    public function save(IPayment $payment): IStorage
    {
        if (true !== $this->connection->set($payment->getId(), serialize($payment))) {
            throw new Exception\Storage('Save error - cant save.');
        }
        return $this;
    }

    /**
     * Проверка на существование платежа
     *
     * @param string $paymentId
     * @return bool
     */
    public function has(string $paymentId): bool
    {
        return $this->connection->exists($paymentId);
    }

    /**
     * Получение платежа
     *
     * @param string $paymentId
     * @return IPayment
     * @throws Exception\NotFound
     */
    public function get(string $paymentId): IPayment
    {
        if (!$this->has($paymentId)) {
            throw new Exception\NotFound("Payment with id: $paymentId not found - no record.");
        }

        try {
            $payment = Payment::stringUnserialize($this->connection->get($paymentId));
        } catch (\Exception $exception) {
            throw new Exception\NotFound(
                "Payment with id: $paymentId not found - cant decode. "
                . $exception->getMessage()
            );
        }
        return $payment;
    }

    /**
     * Удаление платежа
     *
     * @param IPayment $payment
     * @return IStorage
     * @throws Exception\Storage
     */
    public function remove(IPayment $payment): IStorage
    {
        if (0 === $this->connection->del($payment->getId())) {
            throw new Exception\Storage('Delete error - cant delete.');
        }
        return $this;
    }
}
