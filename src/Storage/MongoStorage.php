<?php

namespace Avatar4eg\PhpTestTask\Storage;

use MongoDB\Client;
use MongoDB\Driver\Exception\InvalidArgumentException as DriverInvalidArgumentException;
use MongoDB\Driver\Exception\RuntimeException as DriverRuntimeException;
use MongoDB\Driver\Exception\UnexpectedValueException;
use MongoDB\Exception\InvalidArgumentException;
use MongoDB\Exception\RuntimeException;
use Avatar4eg\PhpTestTask\Payment\IPayment;

class MongoStorage implements IStorage
{
    /** @var  */
    private $connection;

    /**
     * MongoStorage constructor.
     *
     * @param array|null $config
     * @throws Exception\Storage
     */
    private function __construct(array $config = null)
    {
        try {
            if (null === $config) {
                $client = new Client(
                    'mongodb://localhost:27017'
                );
            } else {
                $client = new Client(
                    sprintf('mongodb://%s:%d', $config['host'], $config['port'])
                );
            }
            $this->connection = $client->selectDatabase($config['database']);
        } catch (InvalidArgumentException $exception) {
            throw new Exception\Storage('Connection creation error - bad arguments. ' . $exception->getMessage());
        } catch (DriverRuntimeException $exception) {
            throw new Exception\Storage('Connection creation error - driver error. ' . $exception->getMessage());
        } catch (DriverInvalidArgumentException $exception) {
            throw new Exception\Storage('Connection creation error - bad arguments. ' . $exception->getMessage());
        }
    }

    /**
     * Фабричный метод для создания экземпляра хранилища
     *
     * @param array|null $config
     * @return IStorage
     * @throws Exception\Storage
     */
    public static function instance(array $config = null): IStorage
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new self($config);
        }

        return $inst;
    }

    /**
     * Сохранение существующего платежа или создание нового
     *
     * @param IPayment $payment
     * @return IStorage
     * @throws Exception\Storage
     */
    public function save(IPayment $payment): IStorage
    {
        try {
            if ($this->has($payment->getId())) {
                $result = $this->connection->{'payments'}->updateOne(
                    ['paymentId' => $payment->getId()],
                    ['$set' => $payment]
                );
                if (!$result->isAcknowledged()
                    || ($result->getModifiedCount() === 0 && $result->getMatchedCount() === 0)) {
                    throw new Exception\Storage('Save error - cant save.');
                }
            } else {
                $result = $this->connection->{'payments'}->insertOne($payment);
                if (!$result->isAcknowledged()
                    || $result->getInsertedCount() === 0) {
                    throw new Exception\Storage('Save error - cant save.');
                }
            }
        } catch (DriverRuntimeException $exception) {
            throw new Exception\Storage('Save error - driver error. ' . $exception->getMessage());
        } catch (InvalidArgumentException $exception) {
            throw new Exception\Storage('Save error - bad arguments. ' . $exception->getMessage());
        }

        return $this;
    }

    /**
     * Проверка на существование платежа
     *
     * @param string $paymentId
     * @return bool
     */
    public function has(string $paymentId): bool
    {
        try {
            $result = $this->connection->{'payments'}->count(['paymentId' => $paymentId]);
            return $result && $result > 0;
        } catch (DriverRuntimeException $exception) {
            return false;
        } catch (InvalidArgumentException $exception) {
            return false;
        } catch (UnexpectedValueException $exception) {
            return false;
        }
    }

    /**
     * Получение платежа
     *
     * @param string $paymentId
     * @return IPayment
     * @throws Exception\NotFound
     */
    public function get(string $paymentId): IPayment
    {
        try {
            $result = $this->connection->{'payments'}->findOne(['paymentId' => $paymentId]);
        } catch (RuntimeException $exception) {
            throw new Exception\NotFound("Payment with id: $paymentId not found - mongo error.");
        } catch (DriverRuntimeException $exception) {
            throw new Exception\NotFound("Payment with id: $paymentId not found - connection error.");
        } catch (InvalidArgumentException $exception) {
            throw new Exception\NotFound("Payment with id: $paymentId not found - bad request arguments.");
        }
        if (null === $result || !($result instanceof IPayment)) {
            throw new Exception\NotFound("Payment with id: $paymentId not found - no record.");
        }

        return $result;
    }

    /**
     * Удаление платежа
     *
     * @param IPayment $payment
     * @return IStorage
     * @throws Exception\Storage
     */
    public function remove(IPayment $payment): IStorage
    {
        try {
            if ($this->has($payment->getId())) {
                $result = $this->connection->{'payments'}->deleteOne(['paymentId' => $payment->getId()]);
                if (!$result->isAcknowledged()
                    || $result->getDeletedCount() === 0) {
                    throw new Exception\Storage('Delete error - cant delete.');
                }
            } else {
                throw new Exception\Storage('Cant delete not loaded payment.');
            }
        } catch (DriverRuntimeException $exception) {
            throw new Exception\Storage('Delete error - driver error. ' . $exception->getMessage());
        } catch (InvalidArgumentException $exception) {
            throw new Exception\Storage('Delete error - bad arguments. ' . $exception->getMessage());
        }

        return $this;
    }
}
