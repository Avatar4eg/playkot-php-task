<?php

return [
    'db_type'   => 'redis', // Storage type: 'mongodb' or 'redis'
    'mongodb'   => [
        'host'      => 'localhost',
        'port'      => '27017',
        'database'  => 'payment',
    ],
    'redis'     => [
        'host'      => 'localhost',
        'port'      => '6379',
        'database'  => 'payment',
    ],
];
