<?php

namespace Test\Avatar4eg\PhpTestTask\Storage;

use PHPUnit\Framework\TestCase;
use Avatar4eg\PhpTestTask\Payment\Currency;
use Avatar4eg\PhpTestTask\Payment\IPayment;
use Avatar4eg\PhpTestTask\Payment\State;
use Avatar4eg\PhpTestTask\Storage\IStorage;
use Avatar4eg\PhpTestTask\Storage\MongoStorage;
use Avatar4eg\PhpTestTask\Storage\RedisStorage;
use Avatar4eg\PhpTestTask\Payment\Payment;
use Avatar4eg\PhpTestTask\Storage\Exception;

class StorageTest extends TestCase
{
    public function testCreateStorage()
    {
        $config = require __DIR__ . '/../../config/config.dev.php';

        switch ($config['db_type']) {
            case 'mongodb':
                self::assertTrue(class_exists('Avatar4eg\PhpTestTask\Storage\MongoStorage'), 'Storage class not found');
                self::assertTrue(in_array(IStorage::class, class_implements(MongoStorage::class)), 'Storage not implements IStorage interface');

                $storage = MongoStorage::instance($config['mongodb']);
                self::assertInstanceOf(MongoStorage::class, $storage);
                break;
            case 'redis':
                self::assertTrue(class_exists('Avatar4eg\PhpTestTask\Storage\RedisStorage'), 'Storage class not found');
                self::assertTrue(in_array(IStorage::class, class_implements(RedisStorage::class)), 'Storage not implements IStorage interface');

                $storage = RedisStorage::instance($config['redis']);
                self::assertInstanceOf(RedisStorage::class, $storage);
                break;
            default:
                throw new \RuntimeException('No DB configuration provided');
        }

        for ($i = 1; $i < 10; $i++) {

            $paymentId = 'payment_' . $i;

            $payment = Payment::instance(
                $paymentId,
                new \DateTime('2017-01-02 03:04:05'),
                new \DateTime('2017-02-03 04:05:06'),
                (bool)($i % 2),
                Currency::get(Currency::USD),
                14.55 * $i,
                1.34 * $i,
                State::get($i % 4)
            );

            self::assertFalse($storage->has($paymentId), 'Payment already exists');

            try {
                $storage->get($paymentId);
                $this->fail('Payment NotFound exception expected');
            } catch (\Throwable $e) {
                $this->assertInstanceOf(Exception\NotFound::class, $e, 'Expected NotFound exception');
            }

            // Create
            self::assertSame($storage, $storage->save($payment));
            self::assertTrue($storage->has($paymentId), 'Payment saved but not exists');

            // Read
            self::assertInstanceOf(IPayment::class, $storage->get($paymentId));
            self::assertNotSame($payment, $storage->get($paymentId), 'Payment instance same storage instance');
            self::assertEquals($payment, $storage->get($paymentId), 'Payment instance not equals storage instance');

            // Update
            self::assertSame($storage, $storage->save($payment));
            self::assertEquals($payment, $storage->get($paymentId), 'Payment not modified but changed');

            $changedPayment = Payment::instance(
                $paymentId,
                new \DateTime('2017-03-04 05:06:07'),
                new \DateTime('2017-02-03 04:05:06'),
                $i % 2 ? false :true,
                Currency::get(Currency::RUB),
                12.22 * $i,
                2.54 * $i,
                State::get((int)($i % 4))
            );

            self::assertSame($storage, $storage->save($changedPayment));
            self::assertEquals($changedPayment, $storage->get($paymentId), 'Payment was modified but not changed');

            // Delete
            self::assertSame($storage, $storage->remove($payment));
            self::assertFalse($storage->has($paymentId), 'Payment not removed');
            try {
                $storage->get($paymentId);
                $this->fail('Payment NotFound exception expected');
            } catch (\Throwable $e) {
                $this->assertInstanceOf(Exception\NotFound::class, $e, 'Expected NotFound exception');
            }
        }
    }
}
