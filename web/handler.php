<?php

require_once __DIR__ . '/../vendor/autoload.php';

$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

switch ($request_uri[0]) {
    case '/':
        if ('POST' !== $_SERVER['REQUEST_METHOD']) {
            header('HTTP/1.0 405 Method Not Allowed');
            echo 'Method Not Allowed.';
            break;
        }

        $config = require __DIR__ . '/../config/config.dev.php';

        $content = file_get_contents('php://input');
        $content = json_decode($content, true);
        if (null === $content
            || !array_key_exists('events', $content)
            || !is_array($content['events'])) {
            header('HTTP/1.0 400 Bad Request');
            echo 'Bad request data.';
            break;
        }

        try {
            $handler = new \Avatar4eg\PhpTestTask\Handler\Handler($config);
        } catch (\Avatar4eg\PhpTestTask\Storage\Exception\Payment $exception) {
            header('HTTP/1.0 500 Internal Server Error');
            echo 'DB connection error. ' . $exception->getMessage();
            break;
        }

        $result = [
            'processed'     => [],
            'not_processed' => [],
        ];
        foreach ($content['events'] as $key => $event) {
            $id = array_key_exists('id', $event) ? $event['id'] : "event_n_$key";
            try {
                $handler->process($event);
                $result['processed'][] = $id;
            } catch (\Avatar4eg\PhpTestTask\Storage\Exception\Payment $exception) {
                $result['not_processed'][] = $id;
            }
        }

        header('HTTP/1.0 200 OK');
        echo 'Processed: '
            . implode(', ', $result['processed'])
            . '. Not processed: '
            . implode(', ', $result['not_processed'])
            . '.';

        break;
    default:
        header('HTTP/1.0 404 Not Found');
        echo 'Bad request URL.';

        break;
}
